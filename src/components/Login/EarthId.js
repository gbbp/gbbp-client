"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listenForUserData = exports.generateQrCode = exports.saveUserDataToCookies = void 0;
const axios_1 = __importDefault(require("axios"));
const socket_io_client_1 = __importDefault(require("socket.io-client"));
const universal_cookie_1 = __importDefault(require("universal-cookie"));
let cookies = new universal_cookie_1.default();
const api = "https://server.myearthid.ml";
const socketUrl = "https://socket.myearthid.ml";
const apiKey = "LIFVVEXUTCP";
var socketId;
var socket;
function socketConnection() {
    return new Promise((resolve, reject) => {
        socket = socket_io_client_1.default.connect(socketUrl);
        socket.on('connect', () => {
            socketId = socket.id;
            resolve(socketId);
        });
    });
}
//code to save data in cookies
function saveUserDataToCookies(userData) {
    //TODO remove hard coded values
    const params = {
        id: userData.earthId,
        Name: userData.fname,
        GBAId: 34342,
        StatusId: 2,
        PoAAddr: 'Abcd',
    };
    cookies.set('userInfo', params, { path: '/', maxAge: 10000000 });
}
exports.saveUserDataToCookies = saveUserDataToCookies;
//qrcode generation funcion
function generateQrCode() {
    return __awaiter(this, void 0, void 0, function* () {
        yield socketConnection();
        return new Promise((resolve, reject) => {
            try {
                if (!apiKey) {
                    return new Error("apiKey not availaible");
                }
                if (!socketId) {
                    return new Error("socketId not availaible");
                }
                //get session key from server and store the socketID with session key in db , to be used further during service Provider API to emit
                axios_1.default.get(`${api}/authorize/generateqr?apiKey=${apiKey}&socketId=${socketId}`)
                    .then(response => {
                    if (response.data.code === 400) {
                        let errorString = response.data.message ? response.data.message : "Something went wrong";
                        return reject(errorString);
                    }
                    let qrData = `{"apikey":"${apiKey}","reqNo":"${response.data.result}","sessionKey":"${response.data.result}","requestType":"login" }`;
                    listenForUserData();
                    return resolve(`https://api.qrserver.com/v1/create-qr-code/?data=${qrData}`);
                })
                    .catch(e => {
                    console.log("Error while generateQrCode:::", e);
                    return reject("unable to get secret Token");
                });
            }
            catch (e) {
                console.log("Error while generateQrCode:::", e);
                return reject(e);
            }
        });
    });
}
exports.generateQrCode = generateQrCode;
//Sample User Data
/*
{
    "fname": "Rahul Rejolut",
    "userEmail": "rahul@rejolut.com",
    "userMobileNo": "+918512806087",
    "dob": "",
    "emailVerified": true,
    "score": 450,
    "earthId": "42707778",
    "mobileVerified": true,
    "duration": "86400000"
  }
*/
function listenForUserData() {
    return __awaiter(this, void 0, void 0, function* () {
        socket.on(`userdata`, (data) => __awaiter(this, void 0, void 0, function* () {
            console.log('listenForUserData ::: ', JSON.stringify(data));
            if (data.newreq) {
                let userData = data.newreq;
                //TODO Step 2 - Verify for GBA membership.
                yield saveUserDataToCookies(userData);
                location.reload();
            }
        }));
    });
}
exports.listenForUserData = listenForUserData;
//# sourceMappingURL=EarthId.js.map