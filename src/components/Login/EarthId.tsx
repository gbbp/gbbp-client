import axios from 'axios';
import io from 'socket.io-client';
import Cookies from "universal-cookie";
let cookies = new Cookies();

const api = "https://server.myearthid.ml";
const socketUrl = "https://socket.myearthid.ml";
const apiKey = "LIFVVEXUTCP"
var socketId;
var socket;

function socketConnection() {
    return new Promise((resolve, reject) => {
        socket = io.connect(socketUrl);
        socket.on('connect', () => {
            socketId = socket.id
            resolve(socketId)
        })
    })
}

//code to save data in cookies
export function saveUserDataToCookies(userData) {
    //TODO remove hard coded values
    const params = {
        id: userData.earthId,
        Name: userData.fname,
        GBAId: 34342,
        StatusId: 2,
        PoAAddr: 'Abcd',
    }
    cookies.set('userInfo', params, { path: '/', maxAge: 10000000 });
}

//qrcode generation funcion
export async function generateQrCode() {
    await socketConnection();
    return new Promise((resolve, reject) => {
        try {
            if (!apiKey) {
                return new Error("apiKey not availaible")
            }
            if (!socketId) {
                return new Error("socketId not availaible")
            }
            //get session key from server and store the socketID with session key in db , to be used further during service Provider API to emit
            axios.get(`${api}/authorize/generateqr?apiKey=${apiKey}&socketId=${socketId}`)
                .then(response => {
                    if (response.data.code === 400) {
                        let errorString = response.data.message ? response.data.message : "Something went wrong"
                        return reject(errorString);
                    }
                    let qrData = `{"apikey":"${apiKey}","reqNo":"${response.data.result}","sessionKey":"${response.data.result}","requestType":"login" }`
                    listenForUserData();
                    return resolve(`https://api.qrserver.com/v1/create-qr-code/?data=${qrData}`);
                })
                .catch(e => {
                    console.log("Error while generateQrCode:::", e)
                    return reject("unable to get secret Token")
                })
        } catch (e) {
            console.log("Error while generateQrCode:::", e)
            return reject(e)
        }
    })
}

//Sample User Data
/* 
{
    "fname": "Rahul Rejolut",
    "userEmail": "rahul@rejolut.com",
    "userMobileNo": "+918512806087",
    "dob": "",
    "emailVerified": true,
    "score": 450,
    "earthId": "42707778",
    "mobileVerified": true,
    "duration": "86400000"
  }
*/

export async function listenForUserData() {
    socket.on(`userdata`, async data => {
        if (data.newreq) {
            let userData = data.newreq;
            //TODO Step 2 - Verify for GBA membership.
            await saveUserDataToCookies(userData)
            location.reload()
        }
    });
}

